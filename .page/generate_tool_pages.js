const { execSync } = require("child_process")
const fs = require("fs")
const path = require("path")
const cwd = process.cwd()

const metadata = JSON.parse(fs.readFileSync(path.resolve(cwd, "metadata.json"), "utf-8"))

let i = Object.keys(metadata).length
for(let v in metadata){
  console.log(v, i)
  metadata[v].weight = i
  i--
}

const tool_name = (folder) => {
  return folder.split(".")[2]
}

const tools = (folder) => {
  const ls = fs.readdirSync(folder ? folder : cwd)
    .filter((a) => path.extname(a) == ".xrnx")
    .map((tool_folder) => path.resolve(folder, tool_folder))
  return ls
}

function generate_tool_page(folder){
  const header_template = [
    `+++`,
    `  title = "TITLE"`,
    `  description = "DESCRIPTION"`,
    `  weight = WEIGHT`,
    `  sign = "SIGN"`,
    `+++`,
  ].join("\n")

  const footer_template = `## [download NAME vVERSION](LINK)`
  const name = tool_name(folder)
  const content_folder = `./content/tools/${name}`
  const xrnx_name = path.basename(folder)

  // folder
  if(!fs.existsSync(content_folder)){
    fs.mkdirSync(content_folder, {recursive : true})
  }
  
  // manifest version
  const manifest = fs.readFileSync(path.resolve(folder, "manifest.xml"), "utf-8")
  let _version = manifest.match(/(?<=<Version>)(.*)(?=<\/Version>)/)
  const version = _version[0] || "0.1"

  // xrnx zip
  execSync(`zip ${xrnx_name} -jr ../${xrnx_name}/*`)
  execSync(`mv ${xrnx_name} ${content_folder}/${xrnx_name}`)

  // hugo page
  const text = fs.readFileSync(path.resolve(folder, "README.md"), "utf-8")
  const lines = text.split("\n")
  lines[0] = header_template
    .replace("TITLE", lines[0].substring(1).trim())
    .replace("WEIGHT", metadata[name] ? metadata[name].weight || 1 : 1)
    .replace("SIGN", metadata[name] ? metadata[name].sign || "" : "")
    .replace("DESCRIPTION", metadata[name] ? metadata[name].description || "" : "")
  lines.push("\n")
  lines.push(
    footer_template
      // .replace("NAME", name)
      .replace("NAME", "")
      .replace("VERSION", version)
      .replace("LINK", xrnx_name)
  )
  const hugo_post = lines.join("\n")
  fs.writeFileSync(`./content/tools/${name}/index.md`, hugo_post)

}


tools(process.argv[2]).map(generate_tool_page)

const home = fs.readFileSync(path.resolve("../README.md"), "utf-8")
fs.writeFileSync("./content/home.md", home.replace("https://renoise.unlessgames.com", ""))
