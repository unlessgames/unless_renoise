# zoner

![](https://forum.renoise.com/uploads/default/original/3X/8/9/89d1a46049a654019e8d9c2f6b9e9ac7c5533e49.gif)

Implements a dialog to generate stacked keyzones for multiple samples distributed along the velocity range or octaves etc.

The dialog can be opened three ways 

- using the right-click menu inside the Keyzones editor view
- pressing the `Open zoner` keybinding (if it's configured)
- from *Tools / zoner*
