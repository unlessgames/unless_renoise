# line_grab

![](https://forum.renoise.com/uploads/default/original/2X/f/f41623205081da888de9f5fa4a058963e799b2ae.gif)

Move whole lines vertically or horizontally into available empty lines.

Provides four key-bindings in *Pattern Editor / Tools*

- Move line up
- Move line right
- Move line down
- Move line left

Additionaly there are some more to move selected DSP devices in a similar manner inside the mixer and sample effects view. Renoise provides keybindings for moving a device inside its own track or chain but it won't let you move devices between them, this tool implements these missing actions.

Two in *Mixer / Edit* to move devices across tracks

- Move Device Right
- Move Device Left

and two in *Sample FX Mixer / Device* to move devices across sample effect chains

- Move Device Up
- Move Device Down

