function clone_note(n)
  return {
    note = n.note_value,
    instrument = n.instrument_value,
    volume = n.volume_value,
    panning = n.panning_value,
    delay = n.delay_value,
    effect_number = n.effect_number_value,
    effect_amount = n.effect_amount_value,
  }
end
function note_data(n, i, v, p, d, en, ea)
  return {
    note = n,
    instrument = i,
    volume = v,
    panning = p,
    delay = d,
    effect_number = en,
    effect_amount = ea,
  }
end

function clone_effect(e)
  return {
    number = e.number_value,
    amount = e.amount_value,
  }
end

function effect_data(n, a)
  return {
    number = n,
    amount = a,
  }
end

function clone_line(line)
  local l = {
    note_columns = {},
    effect_columns = {},
  }
  for i = 1, #line.note_columns do
    table.insert(l.note_columns, i, clone_note(line:note_column(i)))
  end

  for i = 1, #line.effect_columns do
    table.insert(l.effect_columns, i, clone_effect(line:effect_column(i)))
  end
  return l
end

function line_data(n, e)
  return {
    note_columns = n,
    effect_columns = e,
  }
end

function load_note_data_to(d, n)
  n.note_value = d.note
  n.instrument_value = d.instrument
  n.volume_value = d.volume
  n.panning_value = d.panning
  n.delay_value = d.delay
  n.effect_number_value = d.effect_number
  n.effect_amount_value = d.effect_amount
end

function load_effect_data_to(d, e)
  e.number_value = d.number
  e.amount_value = d.amount
end

function load_line_data_to(data, line)
  for i = 1, #data.note_columns do
    local n = data.note_columns[i]
    -- print(data.note_columns[i].note)
    load_note_data_to(n, line.note_columns[i])
  end

  for i = 1, #data.effect_columns do
    local e = data.effect_columns[i]
    -- print(data.effect_columns[i].effect)
    load_effect_data_to(e, line.effect_columns[i])
  end
end
