# beat_select

![](https://forum.renoise.com/uploads/default/original/2X/f/fd965f9bf09a0b1313783a26c2f215ba62c3c810.gif)

Faster selection inside the pattern editor by beats and tracks


Provides four key-bindings in *Pattern Editor / Selection*

- Select until previous beat
- Select until next beat
- Select until previous track
- Select until next track

And an additional two in *Pattern Editor / Navigation*

- Jump one beat down
- Jump one beat up

