These are various tools I am making to extend [Renoise](https://www.renoise.com), the cross-platform [music tracker](https://en.wikipedia.org/wiki/Music_tracker).  
The main goal is to improve a keyboard-based workflow and implement experimental features and methods.

## [download all](https://gitlab.com/unlessgames/unless_renoise/-/archive/master/unless_renoise-master.zip)

* or grab specific ones from [tools](https://renoise.unlessgames.com/tools)

## install

* make sure to extract the **.zip** first if you got all the tools at once
* drop a single **.xrnx** file onto an opened Renoise window
* check out the manuals of the [tools](https://renoise.unlessgames.com/tools) for further instructions

## support

Consider [donating](https://ko-fi.com/unlessgames) to help me create, update and maintain these tools, they are completely free but your help would be greatly appreciated!

You can also

- report bugs [where](#contact) it is most convenient for you
- improve the manual by telling me if something is unclear
- contribute [code](https://gitlab.com/unlessgames/unless_renoise)
- just enjoy using my tools!

## contact

You can reach out to me via the following methods to share bug reports, feature requests, comissions etc.

- find me on the [Renoise forums](https://forum.renoise.com/u/unless) as `unless`
- ping me on the [Renoise discord](https://discord.gg/ctD6gU6) as `unlessgames`
- post an issue on the [gitlab repo](https://gitlab.com/unlessgames/unless_renoise)
- send me an [email](unlessgames@gmail.comm)

## thanks

* [@icasiino](https://www.instagram.com/icasiino) for supporting the project
* [Michael Grogan](https://gitlab.com/Muximori) for fixing some bugs and adding commands to [command_palette](https://renoise.unlessgames.com/tools/command_palette)
