# tint

![](https://forum.renoise.com/uploads/default/original/3X/9/1/91aeeb7d8d9a6fcc5c4d5db6d55bf5573e4138b4.gif)

Highlight the track at the cursor with its color. 

Additional settings can be found in the *Tools/tint* menu

- Adjust the `opacity` of the highlight if it is too much or too little for you
- Batch recolor all (or a selection) of tracks with a rainbow spectrum
- Customize the spectrum to perfect your hue game

![](https://forum.renoise.com/uploads/default/original/3X/8/f/8fa587c33fe2cb34de137f4986104fcf95f5a461.gif)
